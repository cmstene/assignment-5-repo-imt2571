<?php
/**
  * Class for accessing DOM data representation of the contents of a Disney.xml
  * file
  */
class Disney
{
    /**
      * The object model holding the content of the XML file.
      * @var DOMDocument
      */
    protected $doc;

    /**
      * An XPath object that simplifies the use of XPath for finding nodes.
      * @var DOMXPath
      */
    protected $xpath;

    /**
      * param String $url The URL of the Disney XML file
      */
    public function __construct($url)
    {
        $this->doc = new DOMDocument();
        $this->doc->load($url);
        $this->xpath = new DOMXPath($this->doc);
    }

    /**
      * Creates an array structure listing all actors and the roles they have
      * played in various movies.
      * returns Array The function returns an array of arrays. The keys of they
      *               "outer" associative array are the names of the actors.
      *                The values are numeric arrays where each array lists
      *                key information about the roles that the actor has
      *                played. The elments of the "inner" arrays are string
      *                formatted this way:
      *               'As <role name> in <movie name> (movie year)' - such as:
      *               array(
      *               "Robert Downey Jr." => array(
      *                  "As Tony Stark in Iron Man (2008)",
      *                  "As Tony Stark in Spider-Man: Homecoming (2017)",
      *                  "As Tony Stark in Avengers: Infinity War (2018)",
      *                  "As Tony Stark in Avengers: Endgame (2019)"),
      *               "Terrence Howard" => array(
      *                  "As Rhodey in Iron Man (2008)")
      *               )
      */
    public function getActorStatistics()
    {
        $result = array();

        $actorEl = $this->xpath->query('Actors')->item(0);
        $actors = $actorEl->childNodes;

        foreach ($actors as $actor) { //For each actor, 92. Using actorID.

        $actorID = $actor->getAttribute('id');  //Get the actorsID

        $actorName = $this->xpath->query("Actors/Actor/Name/text()")->item(0);
        $actorName->textContent;      //For each actor, find their name.

        //Checks if the actorID is the same as attribute named actor inside /Role.
        $roleEl = $this->xpath->query("Subsidiaries/Subsidiary/Cast/Role[@actor='$actorID']")

        foreach ($roleEl as $role){
             $roleName = $role->getAttribute('name'); //Get the role name
             //Get the moveisname and the movieyear
             $movieName = $role->parentNode->parentNode->getElementsByTagName('Name')->item(0)->textContent;
             $movieYear = $role->parentNode->parentNode->getElementsByTagName('Year')->item(0)->textContent;
             //An array returning an array to display
             $result[$actorName->textContent][] = "As ". $roleName. " in ". $movieName. "(". $movieYear.")";

      }

        return $result;
    }

    /**
      * Removes Actor elements from the $doc object for Actors that have not
      * played in any of the movies in $doc - i.e., their id's do not appear
      * in any of the Movie/Cast/Role/@actor attributes in $doc.
      */
    public function removeUnreferencedActors()
    {

      $actorEl = $this->xpath->query('Actors')->item(0);
      $actors = $actorEl->childNodes;

      foreach($actors as $actor){
      $actorID = $actor->getAttribute('id');  //Get the actorID
      //Checks if the actorID is the same as attribute named actor inside /Role.
      $actorCheck = $this->xpath->query("Subsidiaries/Subsidiary/Cast/Role[@actor='$actorID']")

      if ($actorCheck == FALSE) { //If the actor is not in any movie

        $actorID->parentNode->removeChild($actorID);  //Parent deleting the particular node
      }
    }

    /**
      * Adds a new role to a movie in the $doc object.
      * @param String $subsidiaryId The id of the Disney subsidiary
      * @param String $movieName    The name of the movie of the new role
      * @param Integer $movieYear   The production year of the given movie
      * @param String $roleName     The name of the role to be added
      * @param String $roleActor    The id of the actor playing the role
      * @param String $roleAlias    The role's alias (optional)
      */
    public function addRole($subsidiaryId, $movieName, $movieYear, $roleName,
                            $roleActor, $roleAlias = null)
    {

        $role = $this->doc->createElement('Role');  //Create a new role-element
        //Query the correct placement in the XML document
        $roleElement = $this->xpath->query("Subsidiaries/Subsidiary[@id='$subsidiaryId']/Movie[contains(Name,'$movieName')][contains(Year, '$movieYear')]/Cast")[0];
        $roleElement->appendChild($role); //Append

        //Set the attributes
        $role->setAttribute('name', $roleName);
        $role->setAttribute('alias', $roleAlias);
        $role->setAttribute('actor', $roleActor);

    }
}
?>
